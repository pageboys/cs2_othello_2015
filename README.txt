pageboys’ Othello AI

Roshan: Roshan contributed by writing the code for the minimax. He also helped debug.
Sonia: Sonia contributed by writing the code for the heuristic function, and coming up with the values for the grids of the board. She also helped debug. 

The improvements we made were by changing the heuristic function, by making the corner pieces way more favorable, since they are very important in beating the game. 
We also made the places next to the corner pieces worth negative amount, since if we place a piece there, there is a high probability that the opponent will 
place a piece in the corner, leading us to lose. Our strategy will work because we have made an algorithm to strategically place our next moves in favor of 
us winning.  