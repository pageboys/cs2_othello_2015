#include "player.h"

/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
Player::Player(Side side) {
    // Will be set to true in test_minimax.cpp.
    testingMinimax = false;

    gameBoard = new Board();

    mySide = side;
    if(mySide == WHITE)
        otherSide = BLACK;
    else
        otherSide = WHITE;
}

/*
 * Destructor for the player.
 */
Player::~Player() {
// test comment 
}

/*
 * Compute the next move given the opponent's last move. Your AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * msLeft represents the time your AI has left for the total game,s in
 * milliseconds. doMove() must take no longer than msLeft, or your AI will
 * be disqualified! An msLeft value of -1 indicates no time limit.
 *
 * The move returned must be legal; if there are no valid moves for your side,
 * return NULL.
 */
Move *Player::doMove(Move *opponentsMove, int msLeft) {
    if(opponentsMove != NULL){
        gameBoard->doMove(opponentsMove, otherSide);
    }

    Move * newMove;
    if(!testingMinimax){
        newMove = Heuristic();
    }
    else {
        newMove = Minimax();
    }
    gameBoard->doMove(newMove, mySide);

    return newMove;
}

/*
 * Score function: given a board and the move to be played by a side,
 * calculates the score of the resulting board according to a heuristic
 * and returns the score.
 */
int Player::Score(Board currBoard, Move * currMove, Side side){
    currBoard.doMove(currMove, side);

    int final_score = currBoard.count(mySide) - currBoard.count(otherSide);
    int net = 0;

    //If the move is in a corner, its score gets bumped up.
    if(currMove->isThis(0,0) || currMove->isThis(0,7) || 
       currMove->isThis(7,0) || currMove->isThis(7,7))
        net += 200;

    //If the move is adjacent to a corner, its score gets negated.
    if(currMove->isThis(0,1) || currMove->isThis(1,0) || 
	   currMove->isThis(6,0) || currMove->isThis(7,1) ||
       currMove->isThis(0,6) || currMove->isThis(1,7) ||
       currMove->isThis(6,7) || currMove->isThis(7, 6))
        net -= 1;
    
    //If move is diagonally adjacent to a corner    
    if(currMove->isThis(1,1) || currMove->isThis(6,1) ||
	   currMove->isThis(1,6) || currMove->isThis(6,6))
		net -= 20;

	//If move is on an edge
	if(currMove->isThis(0,2) || currMove->isThis(2,0) ||
	   currMove->isThis(0,5) || currMove->isThis(5,0) || 
	   currMove->isThis(7,2) || currMove->isThis(2,7) ||
	   currMove->isThis(7,5) || currMove->isThis(5,7))
	   net += 5;
	   
    if(side != mySide)
        net *= -1;

    return final_score + net;
}

/*
 * Hueristic function: calculates the score of every possible move
 * according to the hueristic in Score and returns the move with the
 * best score.
 */
Move *Player::Heuristic(){
    if(!gameBoard->hasMoves(mySide))
        return NULL;

    Move *final_move = new Move(0, 0);
    int final_score = -9999;
 
    for (int i = 0; i < 8; i++) {
        for (int j = 0; j < 8; j++) {
            Move * move = new Move(i, j);
            if (gameBoard->checkMove(move, mySide)){
                int newScore = Score(*gameBoard, move, mySide);
                if (newScore > final_score){
                    final_score = newScore;
                    final_move->x = i;
                    final_move->y = j;
                }
            }
        }
    }
    return final_move;
}

/*
 * Minimax function: calculates the lowest possible score that can
 * result from a move being played by the player and then a move being
 * played by the opponent. Returns the move with the best possible
 * lowest score.
 */
Move *Player::Minimax(){
    if(!gameBoard->hasMoves(mySide))
        return NULL;

    Move * bestMove = new Move(0, 0);
    int lowestScore = -99999;

    for(int i = 0; i < 8; i++){
        for(int j = 0; j < 8; j++){
            Move * nextMove = new Move(i, j);
            if (gameBoard->checkMove(nextMove, mySide)) {
                Board nextBoard = *gameBoard->copy();
                int backupScore = Score(nextBoard, nextMove, mySide);
                nextBoard.doMove(nextMove, mySide);
                int nextLowestScore = LowestScore(nextBoard, otherSide);
                if(nextLowestScore == 100000000)
                    nextLowestScore = backupScore;
                
                if(nextLowestScore > lowestScore){
                    bestMove = nextMove;
                    lowestScore = nextLowestScore;
                }
            }
        }
    }
    return bestMove;
}

/*
 * LowestScore function: calculates the lowest possible score that can
 * result from a move being played by a certain side and returns that.
 */
int Player::LowestScore(Board currBoard, Side currSide){
    int lowestScore = 100000000;
    for(int i = 0; i < 8; i++){
        for(int j = 0; j < 8; j++){
            if (currBoard.checkMove(new Move(i, j), currSide)){
                int newScore = Score(currBoard, new Move(i, j), currSide);
                if(newScore < lowestScore){
                    lowestScore = newScore;
                }
            }
        }
    }
    return lowestScore;
}

/*
 * changeBoard function: changes the players board to the given input.
 */
void Player::changeBoard(Board * newBoard){
    gameBoard = newBoard;
}
