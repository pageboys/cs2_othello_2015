#ifndef __PLAYER_H__
#define __PLAYER_H__

#include <iostream>
#include "common.h"
#include "board.h"
using namespace std;

class Player {

public:
    Player(Side side);
    ~Player();
    
    Move *doMove(Move *opponentsMove, int msLeft);
    Move *Heuristic();
	Move *Minimax();

	int Score(Board currBoard, Move * currMove, Side side);
 	int LowestScore(Board currBoard, Side currSide);

 	void changeBoard(Board * newBoard);
    // Flag to tell if the player is running within the test_minimax context
    bool testingMinimax;
 protected:
    Board *gameBoard;
    Side mySide;
    Side otherSide;
};

#endif
